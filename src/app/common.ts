import { Player } from "./player.model";
declare const _: any;

export interface GameState {
  players: Player[];
  dropState?: DropState;
  status: GameStatus | null;
}

export interface State {
  prev: GameState;
  current: GameState;
}
export interface DropEvent {
  column: number;
  player: Player;
}

export enum PLAYER_NUMBER {
  ONE = 1,
  TWO = 2
}

export enum DropState {
  STARTED,
  ENDED
}

export enum GameStatus {
  STARTED,
  ENDED
}

export interface Action {
  type: string;
  payload?: any;
}

export enum DIFICULTY {
  EASY,
  HARD
}
