import { Component } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { filter, map } from "rxjs/operators";
import { GamePlayService } from "../../services/gamePlay/game-play.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  constructor(
    private route: ActivatedRoute,
    private _gamePlaySrv: GamePlayService
  ) {}

  ngOnInit() {
    this.route.queryParams
      .pipe(map(params => params.room))
      .subscribe(roomId => {
        console.log(roomId);
        if (roomId) {
          this._gamePlaySrv.joinGame(roomId);
        }
      });
  }
}
