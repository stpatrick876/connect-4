import { Component, OnInit, QueryList } from "@angular/core";
import { ViewChildren } from "@angular/core";
import { CellComponent } from "../cell/cell.component";
import { AfterViewInit } from "@angular/core";
import { DiscStatus } from "../disc/disc.component";
import { ChangeDetectorRef } from "@angular/core";
import { Cell } from "../cell/cell.model";
import { of, pipe, from, interval } from "rxjs";
import { GameStore } from "../../services/state/store/game-store.service";
import { DropEvent, GameState, State, PLAYER_NUMBER } from "../../common";
import { CommonService } from "../../services/common/common.service";
import { Player } from "../../player.model";
import { GamePlayService } from "../../services/gamePlay/game-play.service";
import { debounceTime } from "rxjs/operators";
import { ViewChild } from "@angular/core";
import { DropzoneComponent } from "../dropzone/dropzone.component";
import { NotificationService } from "../../services/notification/notification.service";
import { actions, NUMBER_OF_COLUMNS, DROP_DURATION } from "../../constants";
import { GameResult } from "../../services/gamePlay/Board";
@Component({
  selector: "connect4-board",
  templateUrl: "./board.component.html",
  styleUrls: ["./board.component.scss"]
})
export class BoardComponent implements OnInit {
  emptyCells = new Array<Cell>(49).fill({} as Cell); // 7 columns * 7rows
  @ViewChildren(CellComponent) cellComponents: QueryList<CellComponent>;
  state: GameState;
  hoveredColumn: number;
  menuOpen: boolean;
  player: Player;
  constructor(
    private cdr: ChangeDetectorRef,
    private _gameStore: GameStore,
    private _commonSrv: CommonService,
    private _gamePlaySrv: GamePlayService,
    private _notificationSrv: NotificationService
  ) {}

  ngOnInit() {
    this._gameStore.gameStore$.subscribe((state: GameState) => {
      this.state = state;
      if (state.status === null && !this.menuOpen) {
        this.toggleMenu(true);
      }
    });

    this._gameStore
      .getActivePlayer()
      .subscribe(player => (this.player = player));

    this.buildEmptyGrid();
    this._commonSrv.discDropped$.subscribe((e: DropEvent) => {
      this.onDiscDrop(e);
    });
  }

  private buildEmptyGrid() {
    this.emptyCells = this.emptyCells.map((cell, i) => {
      const row = Math.floor(i / NUMBER_OF_COLUMNS) + 1;
      const column = (i % NUMBER_OF_COLUMNS) + 1;

      return {
        index: i,
        row,
        column,
        player: null
      };
    });
  }

  onColumnHovered = (e: number) => (this.hoveredColumn = e);

  private highlightWinningCells(cells: Array<{ row: number; column: number }>) {
    const winningCellComps: CellComponent[] = [];
    cells.forEach((c: any) => {
      winningCellComps.push(
        this.cellComponents.filter(
          (cellComp: CellComponent) =>
            cellComp.cell.row === c.row && cellComp.cell.column === c.column
        )[0]
      );
    });

    winningCellComps.forEach((cellComp: CellComponent) => {
      cellComp.highlight();
    });
    console.log(this.cellComponents.map(c => c.cell), cells, winningCellComps);
  }

  onDiscDrop(e: DropEvent) {
    this._gameStore.reduce({ type: actions.START_DROP });
    const columnCells = this.cellComponents.toArray().filter(cellComponent => {
      return cellComponent.cell.column === e.column;
    });
    const nextCellToDropIndex = columnCells
      .filter(columnCell => columnCell.cell.player === null)
      .reverse()
      .map(cellComponent => cellComponent.cell.row - 1)[0];
    if (typeof nextCellToDropIndex == "undefined") return false;
    var counter = 0;
    const dropInterval = setInterval(function() {
      const cellComponent = columnCells[counter];
      cellComponent.cell.player = e.player;

      if (counter < nextCellToDropIndex) {
        cellComponent.setDiscState(DiscStatus.DROPPING);
        counter++;
      } else {
        cellComponent.setDiscState(DiscStatus.DROPPED);
        onDropComplete(cellComponent.cell);
        return;
      }
    }, DROP_DURATION);

    var onDropComplete = (cell: Cell) => {
      const nextPlayer =
        this.player.playerNumber === PLAYER_NUMBER.ONE
          ? PLAYER_NUMBER.TWO
          : PLAYER_NUMBER.ONE;
      clearInterval(dropInterval);
      this._gamePlaySrv
        .doMoveAndCheckGameWon(cell)
        .subscribe((result: GameResult) => {
          if (result.won) {
            this._gameStore.reduce({ type: actions.GAME_OVER });
            this.highlightWinningCells(result.cells);
            this._notificationSrv.notify(
              "Game Over",
              `Player ${this.player.playerNumber} Wins!`
            );
          } else {
            this._gameStore.reduce({ type: actions.END_DROP });
            console.log(nextPlayer, "next player");
            this._notificationSrv.notify(`Ready Player ${nextPlayer}!`);
          }
        });
    };
  }

  toggleMenu(open?: boolean) {
    this.menuOpen = open || !this.menuOpen;
    this._notificationSrv.notify("Menu");
  }
}
