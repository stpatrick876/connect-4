import { Component, OnInit } from "@angular/core";
import { DiscStatus } from "../disc/disc.component";
import { Cell } from "./cell.model";
import { Input } from "@angular/core";
import { ElementRef } from "@angular/core";
import { Renderer2 } from "@angular/core";
import { Player } from "../../player.model";
import { HostBinding } from "@angular/core";
import { DROP_DURATION } from "../../constants";

@Component({
  selector: "connect4-cell",
  templateUrl: "./cell.component.html",
  styleUrls: ["./cell.component.scss"]
})
export class CellComponent implements OnInit {
  discStatus: DiscStatus = DiscStatus.EMPTY;
  @Input() cell: Cell;
  @HostBinding("class.cell-highlighted") highlighted: boolean = false;

  constructor(private el: ElementRef, private r2: Renderer2) {}

  ngOnInit() {}

  public setDiscState(status: DiscStatus) {
    this.discStatus = status;

    this.r2.addClass(this.el.nativeElement.querySelector(".cell"), status);
    setTimeout(() => {
      if (this.discStatus === DiscStatus.DROPPING) {
        this.r2.removeClass(
          this.el.nativeElement.querySelector(".cell"),
          status
        );
        this.cell.player = null;
      }
    }, DROP_DURATION + 100);
  }

  public highlight() {
    this.highlighted = true;
    this.r2.addClass(
      this.el.nativeElement.querySelector(".cell"),
      "cell--highlighted"
    );
  }
}
