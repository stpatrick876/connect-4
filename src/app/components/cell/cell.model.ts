import { Player } from "../../player.model";

export interface Cell {
  index: number;
  column: number;
  row: number;
  player: Player | null
}
