import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { SimpleChanges } from '@angular/core';
import { Player } from '../../player.model';
import { GameStore } from '../../services/state/store/game-store.service';
  export enum DiscStatus {
    DROPPING = 'dropping',
    DROPPED = 'dropped',
    EMPTY = 'empty'
  }
@Component({
  selector: 'connect4-disc',
  templateUrl: './disc.component.html',
  styleUrls: ['./disc.component.scss']
})
export class DiscComponent implements OnInit {
  @Input() status: DiscStatus;
  @Input() player: Player;

  constructor(private _gameStore: GameStore) { }

  ngOnInit() {
  }

}
