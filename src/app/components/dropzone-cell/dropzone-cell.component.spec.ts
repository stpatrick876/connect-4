import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DropzoneCellComponent } from './dropzone-cell.component';

describe('DropzoneCellComponent', () => {
  let component: DropzoneCellComponent;
  let fixture: ComponentFixture<DropzoneCellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DropzoneCellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropzoneCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
