import { Component, OnInit, EventEmitter } from "@angular/core";
import { Output } from "@angular/core";
import { Input } from "@angular/core";
import { CommonService } from "../../services/common/common.service";
import { GameStore } from "../../services/state/store/game-store.service";
import { GameState, DropEvent } from "../../common";
import { SimpleChanges } from "@angular/core";
import { Player, PLAYER_TYPE } from "../../player.model";
import { take } from "rxjs/operators";

@Component({
  selector: "connect4-dropzone-cell",
  templateUrl: "./dropzone-cell.component.html",
  styleUrls: ["./dropzone-cell.component.scss"]
})
export class DropzoneCellComponent implements OnInit {
  @Input() columnIndex: number;
  @Input() color: string;
  constructor(
    private _gameStore: GameStore,
    private _commonSrv: CommonService
  ) {}

  ngOnInit() {}

  dropDisc() {
    this._gameStore
      .getActivePlayer()
      .pipe(take(1))
      .subscribe((player: Player) => {
        //if (player.type == PLAYER_TYPE.HUMAN) {
        this._commonSrv.emitDropEvent({
          player,
          column: this.columnIndex
        });
        //}
      });
  }
}
