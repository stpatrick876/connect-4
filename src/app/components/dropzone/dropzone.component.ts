import { Component, OnInit, EventEmitter } from "@angular/core";
import { Input } from "@angular/core";
import { Player, PLAYER_TYPE, AIPlayer } from "../../player.model";
import { GameStore } from "../../services/state/store/game-store.service";
import { DropState, DIFICULTY } from "../../common";
import { Output } from "@angular/core";
import { delay, take, distinctUntilChanged } from "rxjs/operators";
import { CommonService } from "../../services/common/common.service";
import { NotificationService } from "../../services/notification/notification.service";
import { AfterViewInit } from "@angular/core";
import { ChangeDetectorRef } from "@angular/core";
import { ViewChildren } from "@angular/core";
import { QueryList } from "@angular/core";
import { DropzoneCellComponent } from "../dropzone-cell/dropzone-cell.component";
import { GamePlayService } from "../../services/gamePlay/game-play.service";

@Component({
  selector: "connect4-dropzone",
  templateUrl: "./dropzone.component.html",
  styleUrls: ["./dropzone.component.scss"]
})
export class DropzoneComponent implements OnInit {
  dropZoneCells = new Array(7).fill({}); // 7 columns
  player: Player | AIPlayer = {} as Player;
  previousPlayer: Player | AIPlayer = {} as Player;

  notificationOpen: boolean = false;
  PLAYER_TYPE_enum = PLAYER_TYPE;
  @Output() columnHovered: EventEmitter<number | null> = new EventEmitter();
  @ViewChildren(DropzoneCellComponent) cells: QueryList<DropzoneCellComponent>;

  constructor(
    private _gameStore: GameStore,
    private _notificationSrv: NotificationService,
    private cdr: ChangeDetectorRef,
    private _gamePlaySrv: GamePlayService
  ) {}

  ngOnInit() {
    this._gameStore
      .selectDropState()
      .pipe(delay(1500))
      .subscribe(this.handleDropEvent);

    this._gameStore
      .getActivePlayer()
      .pipe(distinctUntilChanged())
      .subscribe(this.handlePlayerChange);
  }

  private handlePlayerChange = (player: Player) => {
    this.player = player;
    if (player.type === PLAYER_TYPE.MACHINE) {
      let columnIndex: number;
      if ((player as AIPlayer).level === DIFICULTY.EASY) {
        columnIndex = (this.player as AIPlayer).getMove();
        this.makeMachinePlay(columnIndex);
      } else {
        this._gamePlaySrv.getAiMove().subscribe((columnIndex: number) => {
          this.makeMachinePlay(columnIndex);
        });
      }
    }
    this.previousPlayer = player;
  };
  private makeMachinePlay(columnIndex: number): any {
    const cell = this.cells.filter(
      (cellComponent: DropzoneCellComponent) =>
        cellComponent.columnIndex === columnIndex
    )[0];
    setTimeout(() => {
      cell.dropDisc();
    }, 4000);
  }
  onNotificationToggle(isOpen: boolean) {
    this.notificationOpen = isOpen;
    this.cdr.detectChanges();
  }
  private handleDropEvent = (dState: DropState) => {
    if (typeof dState == "undefined") return false;
    this.notificationOpen = dState === DropState.STARTED;
  };
}
