import { Component, OnInit, EventEmitter } from "@angular/core";
import { GameStore } from "../../services/state/store/game-store.service";
import { GameStatus, PLAYER_NUMBER, DIFICULTY } from "../../common";
import { CommonService } from "../../services/common/common.service";
import { Output } from "@angular/core";
import { NotificationService } from "../../services/notification/notification.service";
import { Player, PLAYER_TYPE, AIPlayer } from "../../player.model";
import { actions } from "../../constants";
import { InviteService } from "../../services/invite/invite.service";
import { GamePlayService } from "../../services/gamePlay/game-play.service";

@Component({
  selector: "connect4-menu",
  templateUrl: "./menu.component.html",
  styleUrls: ["./menu.component.scss"]
})
export class MenuComponent implements OnInit {
  @Output() closeMenu: EventEmitter<boolean> = new EventEmitter();
  oponent: PLAYER_TYPE = PLAYER_TYPE.MACHINE;
  PLAYER_TYPE_enum = PLAYER_TYPE;
  DIFICULTY_Enum = DIFICULTY;
  dificulty: DIFICULTY = DIFICULTY.EASY;
  email: string;
  multiplayer: "local" | "invite" = "local";
  constructor(
    private _gameStore: GameStore,
    private _notificationSrv: NotificationService,
    private _commonSrv: CommonService,
    private _gamePlaySrv: GamePlayService
  ) {}

  ngOnInit() {}

  get inputHighlight() {
    return this.email && this.email.replace(/ /g, "\u00a0");
  }

  startGame() {
    let players: [Player, Player] = [] as [Player, Player];

    players[0] = new Player(PLAYER_NUMBER.ONE, true, "blue", PLAYER_TYPE.HUMAN);
    if (this.oponent === PLAYER_TYPE.MACHINE) {
      const aiPlayer = new AIPlayer(
        PLAYER_NUMBER.TWO,
        false,
        "red",
        this.oponent
      );
      aiPlayer.level = this.dificulty;
      players[1] = aiPlayer;
    } else {
      players[1] = new Player(PLAYER_NUMBER.TWO, false, "red", this.oponent);
    }

    this._commonSrv.setupPlayers(players);
    this.closeMenu.emit(false);
    this._gameStore.reduce({ type: actions.START_GAME });
    this._notificationSrv.close();
  }

  onDificultyChange(e: DIFICULTY) {
    this.dificulty = e;
  }

  onMultiplayerChange(e: "local" | "invite") {
    this.multiplayer = e;
  }

  sendInvite() {
    this._gamePlaySrv.createMultiplayerGame(this.email).subscribe(res => {
      console.log("dmd ", res);
    });
  }
}
