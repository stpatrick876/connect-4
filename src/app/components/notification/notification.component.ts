import { Component, OnInit, EventEmitter } from "@angular/core";
import { GameStore } from "../../services/state/store/game-store.service";
import { Player } from "../../player.model";
import "confetti-js";
import { CommonService } from "../../services/common/common.service";
import {
  NotificationService,
  NotificationState
} from "../../services/notification/notification.service";
import { Output } from "@angular/core";
declare const ConfettiGenerator: any;
@Component({
  selector: "connect4-notification",
  templateUrl: "./notification.component.html",
  styleUrls: ["./notification.component.scss"]
})
export class NotificationComponent implements OnInit {
  message: string;
  addGrafiti: boolean;
  player: Player;
  isOpen: boolean;
  messageSubscription: any;
  @Output() toggle: EventEmitter<boolean> = new EventEmitter();
  constructor(
    private _gameStore: GameStore,
    private _commonSrv: CommonService,
    private _notificationSrv: NotificationService
  ) {}

  ngOnInit() {
    this._gameStore
      .getActivePlayer()
      .subscribe((player: Player) => (this.player = player));
    this._notificationSrv.listen(
      this.handleNotificationChange.bind(this),
      this.onNotificationClosed
    );
  }

  onNotificationClosed = () => {
    this.message = "";
    this.messageSubscription.unsubscribe();
  };
  handleNotificationChange(n: NotificationState) {
    this.isOpen = n.open;
    this.toggle.emit(this.isOpen);
    if (this.isOpen) {
      this.messageSubscription = n.message$.subscribe((m: string) => {
        this.message = m;
      });
    }
  }
}
