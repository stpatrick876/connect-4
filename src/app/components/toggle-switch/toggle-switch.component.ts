import { Component, OnInit, EventEmitter } from "@angular/core";
import { Input } from "@angular/core";
import { Output } from "@angular/core";
import { DIFICULTY } from "../../common";

@Component({
  selector: "connect4-toggle-switch",
  templateUrl: "./toggle-switch.component.html",
  styleUrls: ["./toggle-switch.component.scss"]
})
export class ToggleSwitchComponent implements OnInit {
  @Input() rLabel: string;
  @Input() lLabel: string;
  @Input() rValue: any;
  @Input() lValue: any;
  @Input() selected: any;
  @Input() disabled: boolean;

  activeLeft: boolean = true;
  @Output() change: EventEmitter<any> = new EventEmitter();
  constructor() {}

  ngOnInit() {
    this.activeLeft = this.selected == this.lValue;
  }

  toggle() {
    this.activeLeft = !this.activeLeft;
    const value = this.activeLeft ? this.lValue : this.rValue;
    this.change.emit(value);
  }
}
