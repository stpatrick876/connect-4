export const NUMBER_OF_CONNECTIONS_TO_WIN = 4; // TODO: chaneg to 4
export const actions = {
  CHANGE_TURN: "[PLAYER] Change",
  START_DROP: "[DROP] Start",
  END_DROP: "[Drop] End",
  GAME_OVER: "[Game] Over",
  START_GAME: "[Game] Start"
};
export const NUMBER_OF_ROWS = 7;
export const NUMBER_OF_COLUMNS = 7;

export const DROP_DURATION = 300;
