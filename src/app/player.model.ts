import { PLAYER_NUMBER, DIFICULTY } from "./common";
export enum PLAYER_TYPE {
  HUMAN,
  MACHINE
}
export class Player {
  constructor(
    public playerNumber: PLAYER_NUMBER,
    public active: boolean,
    public color: string,
    public type: PLAYER_TYPE
  ) {}
}

export class AIPlayer extends Player {
  level: DIFICULTY;

  getMove(board?: Array<Array<PLAYER_NUMBER>>) {
    var columnNumber = this.getRandomInt(1, 7);
    return columnNumber;
  }

  set dificulty(level: DIFICULTY) {
    this.level = level;
  }

  private getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
  }
}
