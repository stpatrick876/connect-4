import { Injectable } from "@angular/core";
import { Subject, ReplaySubject } from "rxjs";
import { DropEvent } from "../../common";
import { Player } from "../../player.model";

@Injectable({
  providedIn: "root"
})
export class CommonService {
  // Drop Event Data Observables
  private discDropSource = new Subject<DropEvent>();
  discDropped$ = this.discDropSource.asObservable();

  // Players Data Observables
  private playersSource = new Subject<Array<Player>>();
  players$ = this.playersSource.asObservable();

  constructor() {}

  emitDropEvent(e: DropEvent) {
    this.discDropSource.next(e);
  }

  setupPlayers(players: [Player, Player]) {
    this.playersSource.next(players);
  }
}
