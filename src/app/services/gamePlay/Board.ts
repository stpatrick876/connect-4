import { PLAYER_NUMBER } from "../../common";
import { Cell } from "../../components/cell/cell.model";
import { NUMBER_OF_CONNECTIONS_TO_WIN } from "../../constants";
declare const _: any;

export interface GameResult {
  won: boolean;
  cells?: Array<{ row: number; column: number }>;
}

export class Connect4Board {
  board: Array<Array<PLAYER_NUMBER>>;
  constructor() {
    this.board = [
      [null, null, null, null, null, null, null],
      [null, null, null, null, null, null, null],
      [null, null, null, null, null, null, null],
      [null, null, null, null, null, null, null],
      [null, null, null, null, null, null, null],
      [null, null, null, null, null, null, null],
      [null, null, null, null, null, null, null]
    ];
  }

  public syncCellUpdateToBoard(cell: Cell) {
    this.board[cell.row - 1][cell.column - 1] = cell.player.playerNumber;
  }

  public toString() {
    return _
      .flatten(this.board)
      .map(v => {
        if (v === null) {
          return 0;
        } else {
          return v;
        }
      })
      .join("");
  }

  public hasPlayerWon = (cell: Cell): GameResult => {
    let result: GameResult = this.hasVerticalConnection(cell); // check columns
    if (!result.won) {
      // check rows
      result = this.hasHorizontalConnection(cell);
      // if (!result.won) {
      //   throw new Error("implements diogonal check");
      //   //result = this.hasDiagonalConnection(cell);
      // }
    }

    return result;
  };
  //
  // this.hasDiagonalConnection(cell);

  private hasVerticalConnection(cell: Cell): GameResult {
    let { column, player } = cell;
    let consecutiveCells = 0;
    let winningCells = [];
    for (let row = 0; row < this.board.length; ++row) {
      let winningCell: any = {
        row: row + 1
      };
      if (this.board[row][column - 1] == player.playerNumber) {
        winningCell.column = column;
        winningCells.push(winningCell);

        if (++consecutiveCells == NUMBER_OF_CONNECTIONS_TO_WIN) {
          return {
            won: true,
            cells: winningCells
          };
        }
      } else {
        consecutiveCells = 0;
      }
    }
    return {
      won: false
    };
  }

  private hasHorizontalConnection(cell: Cell): GameResult {
    let consecutiveCells = 0;
    let { row, player } = cell;
    let winningCells = [];

    for (var column = 0; column < this.board[0].length; ++column) {
      let winningCell: any = {
        column: column + 1
      };
      if (this.board[row - 1][column] == player.playerNumber) {
        winningCell.row = row;
        winningCells.push(winningCell);
        if (++consecutiveCells == NUMBER_OF_CONNECTIONS_TO_WIN) {
          return {
            won: true,
            cells: winningCells
          };
        }
      } else {
        consecutiveCells = 0;
      }
    }
    return {
      won: false
    };
  }

  private hasDiagonalConnection = (cell: Cell): GameResult => {
    let result: GameResult = this.hasPositiveDiagonalConnection(cell); // check columns
    if (!result.won) {
      result = this.hasNegativeDiagonalConnection(cell);
    }
    return result;
  };

  private hasPositiveDiagonalConnection(cell: Cell): GameResult {
    let { row, column, player } = cell;
    let initialRow = row - 1;
    let initialColumn = column - 1;
    let winningCells = [];

    while (initialRow > 0 && initialColumn > 0) {
      initialRow--;
      initialColumn--;
    }

    var consecutiveCells = 0;

    for (
      var i = initialRow, j = initialColumn;
      i < this.board.length && j < this.board[0].length;
      ++i, ++j
    ) {
      if (this.board[i][j] == player.playerNumber) {
        winningCells.push({
          row: j + 1,
          column: i + 1
        });

        if (++consecutiveCells == NUMBER_OF_CONNECTIONS_TO_WIN) {
          return {
            won: true,
            cells: winningCells
          };
        }
      } else {
        consecutiveCells = 0;
      }
    }

    return {
      won: false
    };
  }

  private hasNegativeDiagonalConnection(cell: Cell): GameResult {
    let { row, column, player } = cell;

    var initialRow = row - 1;
    var initialColumn = column - 1;
    let winningCells = [];

    while (initialRow > 0 && initialColumn < this.board[0].length - 1) {
      initialRow--;
      initialColumn++;
    }

    var consecutiveCells = 0;

    for (
      var i = initialRow, j = initialColumn;
      i < this.board.length && j >= 0;
      ++i, --j
    ) {
      if (this.board[i][j] == player.playerNumber) {
        winningCells.push({
          row: j + 1,
          column: i + 1
        });
        if (++consecutiveCells == NUMBER_OF_CONNECTIONS_TO_WIN) {
          return {
            won: true,
            cells: winningCells
          };
        }
      } else {
        consecutiveCells = 0;
      }
    }

    return {
      won: false
    };
  }
}
