import { Injectable } from "@angular/core";
import { Cell } from "../../components/cell/cell.model";
import { Player } from "../../player.model";
import { of, Observable } from "rxjs";
import { PLAYER_NUMBER } from "../../common";
import { Connect4Board, GameResult } from "./Board";
import { HttpClient } from "@angular/common/http";
import { map, tap, switchMap } from "rxjs/operators";
import { SocketIo } from "ng-io";
import { InviteService } from "../invite/invite.service";

@Injectable({
  providedIn: "root"
})
export class GamePlayService {
  connect4Board: Connect4Board;

  constructor(
    private _http: HttpClient,
    private socket: SocketIo,
    private _inviteSrv: InviteService
  ) {
    this.connect4Board = new Connect4Board();
  }

  getAiMove(): any {
    return this._http
      .get(
        `http://kevinalbs.com/connect4/back-end/index.php/getMoves?board_data=${this.connect4Board.toString()}&player=2`
      )
      .pipe(
        map(obj => {
          let bestMove = 0;
          for (let key in obj) {
            const val = obj[key];
            const lVal = obj[bestMove];

            if (val > lVal) {
              bestMove = Number.parseInt(key);
            }
          }
          return bestMove + 1;
        })
      );
  }

  doMoveAndCheckGameWon(cell: Cell): Observable<GameResult> {
    this.connect4Board.syncCellUpdateToBoard(cell);
    return of(this.connect4Board.hasPlayerWon(cell));
  }

  createMultiplayerGame(email: string) {
    console.log("creating game");
    this.socket.emit("createGame");
    return this.socket.fromEvent("newGame").pipe(
      tap(() => this.onOponentJoined()),
      switchMap((roomId: string) => this._inviteSrv.sendInvite(email, roomId))
    );
  }

  onOponentJoined() {
    this.socket.fromEvent("oponentJoined").subscribe(d => {
      console.log("oponent joined ", d);
    });

    this.socket.fromEvent("notifications").subscribe(d => {
      console.log("notifications ", d);
    });
  }

  joinGame(roomId: string) {
    this.socket.emit("joinGame", { roomId });
  }
}
