import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class InviteService {
  constructor(private _http: HttpClient) {}

  sendInvite(email: string, roomId: string) {
    const apiUrl = `https://jb5ymta037.execute-api.us-east-1.amazonaws.com/dev/sendEmailInvite?email=${email}&room=${roomId}`;
    return this._http.get(apiUrl);
  }
}
