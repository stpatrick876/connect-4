import { Injectable } from "@angular/core";
import { Subject, timer, empty, of, Observable, ReplaySubject } from "rxjs";
import { switchMap, tap } from "rxjs/operators";
export interface NotificationState {
  open: boolean;
  message$?: Observable<string>;
}
@Injectable({
  providedIn: "root"
})
export class NotificationService {
  messageQueue: Array<string> = ["hello"];

  private notifSource: Subject<NotificationState> = new ReplaySubject<
    NotificationState
  >(1);

  private messageSource: Subject<string> = new ReplaySubject<string>(1);
  private messageIntervalSubscription: any;
  onCloseCb: any;
  shouldPause: boolean;
  open: boolean;
  duration: number = 5000;
  constructor() {}
  private addMessages(...messages: string[]) {
    this.messageQueue = [...messages];
    this.messageIntervalSubscription = timer(0, this.duration)
      .pipe(
        switchMap(this.handleMessageScan),
        tap(m => {
          this.messageSource.next(m);
        })
      )
      .subscribe();
  }

  private handleMessageScan = t => {
    if (this.shouldPause) {
      return empty();
    } else {
      const [next] = [...this.messageQueue];
      this.messageQueue.splice(0, 1);
      this.messageQueue.push(next);
      return of(next);
    }
  };
  listen(subCb: any, onCloseCb: any) {
    this.notifSource.asObservable().subscribe(subCb);
    this.onCloseCb = onCloseCb;
  }

  notify(...messages: string[]) {
    if (this.open) {
      this.close(false);
    }
    this.open = true;
    this.notifSource.next({
      open: this.open,
      message$: this.messageSource.asObservable()
    });

    this.addMessages(...messages);
  }
  close(hardClose = true) {
    this.messageIntervalSubscription.unsubscribe();
    this.messageQueue.length = 0;
    this.onCloseCb();
    if (hardClose) {
      this.open = false;
      this.notifSource.next({ open: this.open });
    }
  }

  toggleMessage() {
    this.shouldPause = !this.shouldPause;
  }
}
