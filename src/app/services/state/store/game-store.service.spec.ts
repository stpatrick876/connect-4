import { TestBed, inject } from '@angular/core/testing';

import { GameStoreService } from './game-store.service';

describe('GameStoreService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GameStoreService]
    });
  });

  it('should be created', inject([GameStoreService], (service: GameStoreService) => {
    expect(service).toBeTruthy();
  }));
});
