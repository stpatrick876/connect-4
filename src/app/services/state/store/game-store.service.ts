import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable, of } from "rxjs";
import {
  GameState,
  DropState,
  Action,
  State,
  PLAYER_NUMBER,
  GameStatus
} from "../../../common";
import { CommonService } from "../../common/common.service";
import { Player } from "../../../player.model";

import { switchMap, map } from "rxjs/operators";
import { actions } from "../../../constants";

@Injectable({
  providedIn: "root"
})
export class GameStore {
  private gameStates: GameState = {
    players: [],
    status: null
    //  dropState: DropState.ENDED
  };
  private _gameStore$: BehaviorSubject<GameState> = new BehaviorSubject(
    this.gameStates
  );

  public readonly gameStore$: Observable<
    GameState
  > = this._gameStore$.asObservable();

  constructor(private _commonSrv: CommonService) {
    _commonSrv.players$.subscribe((players: Array<Player>) => {
      this.updateState({ ...this.gameStates, players });
    });
  }

  private updateState(newState: GameState) {
    this.gameStates = newState;
    this._gameStore$.next(newState);
  }

  public reduce(action: Action) {
    let newState: GameState = {} as GameState;
    switch (action.type) {
      case actions.START_DROP:
        this.updateState({
          ...this.gameStates,
          dropState: DropState.STARTED
        });
        break;

      case actions.END_DROP:
        this.switchPlayers(this.gameStates).subscribe((players: Player[]) => {
          this.updateState({
            ...this.gameStates,
            dropState: DropState.ENDED,
            players
          });
        });

        break;

      case actions.GAME_OVER:
        this.updateState({
          ...this.gameStates
        });
        break;

      case actions.START_GAME:
        this.updateState({
          ...this.gameStates,
          status: GameStatus.STARTED
        });
        break;

      default:
        return this.gameStates;
    }
  }

  public getActivePlayer = () =>
    this.gameStore$.pipe(
      switchMap((state: GameState) =>
        state.players.filter((player: Player) => player.active === true)
      )
    );

  public selectDropState = () =>
    this.gameStore$.pipe(map((state: GameState) => state.dropState));

  public selectGameStatus = () =>
    this.gameStore$.pipe(map((state: GameState) => state.status));

  private switchPlayers = (state: GameState): Observable<Array<Player>> => {
    return of(
      state.players.map((player: Player) => {
        player.active = !player.active;
        return player;
      })
    );
  };
}
